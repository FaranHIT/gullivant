<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/contact', function () {
    return view('contactus');
});

Route::get('/accomodation', function () { return view('accomodation');});
Route::get('/search-place', function () { return view('search-place');});
Route::get('/trip', function () { return view('trip');});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
