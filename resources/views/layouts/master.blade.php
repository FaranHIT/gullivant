<!DOCTYPE HTML>
<html>
  <head>
     <title>@yield('title')</title>
     @include('include.header')
  </head>
  <body>
    @yield('content')

    @include('include.footer')
  </body>
</html>