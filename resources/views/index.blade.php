@extends('layouts.master')

@section('content')
    <header class="site-header js-site-header">
        @include('page_contents.home.header')
    </header>
    <!-- END head -->

    <section class="site-hero overlay" style="background-image: url(images/here_5.jpg)" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row">
          <div class="col-md-10" data-aos="fade-up">
            <h1 class="heading">The Best Hotel</br>your will ever need</h1>
            <span class="custom-caption text-uppercase text-white d-block  mb-3">Welcome To 5 <span class="fa fa-star text-primary"></span>   Hotel</span>

          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

    <section class="section pb-0"  >
      <div class="container">
        <div class="row check-availabilty" id="next">
          <div class="block-32" data-aos="fade-up" data-aos-offset="-200">
            <!-- fds -->
            <div class="container">
              <div class="row">
                <div class="col-sm-12">
                  <form class="form-inline">
                    <div class="row">
                      <div class="col-md-2 form-group">
                          <label for="">Full Name</label></br>
                          <input type="text" class="form-control" id="exampleInputEmail3" placeholder="Full Name">
                      </div>
                      <div class="col-md-2 form-group">
                        <label for="">Travel Istinsations</label></br>
                        <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Add details">
                      </div>
                      <div class="col-md-2 form-group">
                        <label for="">Start date</label></br>
                        <input type="date" class="form-control" id="exampleInputEmail3" placeholder="00000000">
                      </div>
                      <div class="col-md-2 form-group">
                        <label for="">End Date</label></br>
                        <input type="date" class="form-control" id="exampleInputEmail3" placeholder="00000000">
                      </div>
                      <div class="col-md-4 form-group">
                      <label for=""></label></br>
                        <button class="btn-hotel btn-danger btn-lg">Create Plan</button>
                      </div>
                    </div>  
                  </form>
                </div>
              </div>
            </div>
            <!-- demd -->
          </div>
        </div>
      </div>
    </section>

    <section class="py-5" >
      <div class="container">
         <div class="row">
            <div class="col-md-6">
              <h2 data-aos="fade-up" data-aos-offset="-200">The best comfort food will always <br>be greens, cornbread, and fried chickens</h2><br>
              <p data-aos="fade-up" data-aos-offset="-200">
              d ldskf sd;lfklds;kfl;dsfk l;sdfkldsfk l;dsfk ldsfk lds;f kldskf l;dsfkd
              dsfldsf ldkfldsfdjfkdsj fldskjf lkdsf jldksf jdlksjf dlksjf kldsjf kdslj fdksl
              dsfjd klfj sdlk jkdslf jkldsjf kldsjf kldsf jkdlsjf ksdlf jkdslfj lkdsfsjd lkdsfsjdd
              dfkdf kldjf kldsj fkldjf kldsfjkldsf jkldsj fklds jfdlksjfkldsfj
              dsf kdslfjldsfkldjsf kldsjfkldsjf kldsjfkldsjf kldsjfkldsjf lkdsjfklsdjf kldsjfkldsjf
              dfk dfjslkfjkldsjf kldjf ldks jfkldsjf kldsjf kldsj lkdsjf klsdjf lkdsjfklsdjf
              dskda lj fldskajf lksdjf alkdjfksdljf kldsfj dlskjflksd jfklsdjf kdfslj aslkdf jkldsjd
              dsfkdslfkajdf lkdsjf lksdfjdskljfkdsljfkldsjf kdlsjfkdlsjf kldsjf lkdsfj 
              </p><br>
              <button class="btn btn-danger btn-lg">Know More About us</button>
            </div>
            <div class="col-md-6" data-aos="fade-up" data-aos-offset="-200">
             <img src="images/here_5.jpg" height="460" width="550" alt="">
            </div>
         </div>
      </div>
    </section>
  
    <section>
    <div class="container" style="background-image: url(images/ic_news_bg.png);height:350px;margin-bottom:2%;">
         <div class="row justify-content-center">
              <div class="col-md-6">
                   <div class="news_image">
                        <h2>Lets Stay in Touch</h2>
                        <p>ddkfj ldsfjk ldfkdfkldf afj dsla</p>
                        <form>
                            <div class="form-group newlette">
                                <input type="email" placeholder="Enter your email">
                                <span class="input-group-btn">
                                    <button class="btn-danger" type="submit">Subscribe Now</button>
                                </span>
                            </div>
                        </form>
                   </div>
              </div>
              <div class="col-md-6">
                 <img src="images/ic_img.png " class="news_image" width="300"   alt="">
              </div>
         </div>
    </div>
    </section>
 
    <section class="section-four">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
             <h3 class="section-four-header">The Best comfort with always be green,
             cornbread and fired chickens.</h3>
              <p class="section-four-p">kdsj dlsfj ksdlaf jlsdkfj kldsjf kldsjf kdsljf klsddfj
                    dsaljd klfdja sdlkfj kdslafj lksdfj klsdafj dlkfj dkslfj
                    dksajf sldfj kdsljf lkdsaf jdksljf klsd fajklsdf jkldsjf
                    adsf ldsfd lksjf aldsjfk ldsjf kldajf sklfj ksdlafj kldfj fjdjf
                    dsakjfl adsfkj dslkajf alsdkjf laksdjf lkdsjlk lkajsd
                    asklf jsdkljf klsadj fklasjf lksdajf.</p>
          </div>
          <div class="col-md-4">
            <form action="">
               <input type="text" class="form-control">
               <input type="text" class="form-control section_form">
               <input type="text" class="form-control section_form">
               <textarea name="" id="" cols="30" rows="10" class="form-control section_form" ></textarea>
               <button class="btn-danger form-control">Submit</button>
              </form>
          </div>
        </div>
      </div>
    </section>

    <div class="footer">
      <div class="container">
          <div class="row">
              <div class="col-md-6">
                  <h5>FLdlfd dksksdajfdskfj alsd </h5></br>
                  <p>kdsj dlsfj ksdlaf jlsdkfj kldsjf kldsjf kdsljf klsddfj
                    dsaljd klfdja sdlkfj kdslafj lksdfj klsdafj dlkfj dkslfj
                    dksajf sldfj kdsljf lkdsaf jdksljf klsd fajklsdf jkldsjf
                    adsf ldsfd lksjf aldsjfk ldsjf kldajf sklfj ksdlafj kldfj fjdjf
                    dsakjfl adsfkj dslkajf alsdkjf laksdjf lkdsjlk lkajsd
                    asklf jsdkljf klsadj fklasjf lksdajf 
                  </p>
              </div>
              <div class="col-md-2">
                 <h5>FLdlfd dalsd </h5><br>
                  <ul>
                     <li class="nav-item">AboutUS</li>
                     <li class="nav-item">AboutUS</li>
                     <li class="nav-item">AboutUS</li>
                     <li class="nav-item">AboutUS</li>
                  </ul>
              </div>
              <div class="col-md-2">
                 <h5>FLdlfd dalsd </h5><br>
                  <ul>
                     <li class="nav-item">AboutUS</li>
                     <li class="nav-item">AboutUS</li>
                     <li class="nav-item">AboutUS</li>
                     <li class="nav-item">AboutUS</li>
                  </ul>
              </div>
              <div class="col-md-2">
                 <h5>FLdlfd dalsd </h5><br>
                  <ul>
                     <li class="nav-item">AboutUS</li>
                     <li class="nav-item">AboutUS</li>
                     <li class="nav-item">AboutUS</li>
                     <li class="nav-item">AboutUS</li>
                  </ul>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <h5>Download our app</h5>
                   <img src="images/ic_app_store.png" alt="">
                   <img src="images/ic_play-store.png" alt="">
              </div>
          </div>
      </div>
    </div>
    
@endsection