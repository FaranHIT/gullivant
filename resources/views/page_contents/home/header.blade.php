<div class="container">
        <div class="row align-items-center">
           <nav class="navbar navbar-expand-md navbar-dark fixed-top" id="banner">
            <div class="container">
            <!-- Brand -->
            <a class="navbar-brand" href="#"><img src="images/ic_login_logo.png" alt=""></a>

            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
              <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                  <a class="nav-link" href="{{url('/')}}">HOME</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('search-place')}}">TRAVEL PLANS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('accomodation')}}">Accomodation</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('trip')}}">Trip</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('contact')}}">CONTACT</a>
                </li> 
                <li class="nav-item">
                  <a class="nav-link" href="#">MY ACCOUNTS</a>
                </li>
              <!-- Dropdown -->
              <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                  Dropdown link
                </a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="#">Link 1</a>
                  <a class="dropdown-item" href="#">Link 2</a>
                  <a class="dropdown-item" href="#">Link 3</a>
                </div>
              </li> -->
              </ul>
            </div>
            </div>
          </nav>
        </div>
      </div>