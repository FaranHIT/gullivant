<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Hotel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=|Roboto+Sans:400,700|Playfair+Display:400,700">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">
    <link rel="stylesheet" href="css/fancybox.min.css">
    
    <link rel="stylesheet" href="fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome/css/font-awesome.min.css">

    <!-- Theme Style -->
    <link rel="stylesheet" href="css/style.css">
  <style>
    .navbar-nav .nav-item .nav-link{
          padding: 1.1em 1em!important;
          font-size: 80%;
            font-weight: 500;
            letter-spacing: 1px;
            color: black;
           font-family: 'Gothic A1', sans-serif;
        }
  </style>
  </head>
  <body>
    
    <header class="site-header js-site-header">
      <div class="container">
        <div class="row align-items-center">
           <nav class="navbar navbar-expand-md navbar-dark fixed-top" id="banner">
            <div class="container">
            <!-- Brand -->
            <a class="navbar-brand" href="#"><span>Logo</span> Here</a>

            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
              <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                  <a class="nav-link" href="#">HOME</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">TRAVEL PLANS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">ABOUTUS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">CONTACT</a>
                </li> 
                <li class="nav-item">
                  <a class="nav-link" href="#">MY ACCOUNTS</a>
                </li>
              <!-- Dropdown -->
              <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                  Dropdown link
                </a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="#">Link 1</a>
                  <a class="dropdown-item" href="#">Link 2</a>
                  <a class="dropdown-item" href="#">Link 3</a>
                </div>
              </li> -->
              </ul>
            </div>
            </div>
          </nav>
        </div>
      </div>
    </header>
    <!-- END head -->

     <div class="form container" >
         <div class="row">
             <div class="col-md-6">
                 <div class="row">
                    <div class="col-md-12">
                      <h3>Munually add accomodation</h3>
                    </div>
                 </div>
                 <div class="row">
                   <div class="col-md-12 full-width">
                      <form class="form-inline">
                        <input class="form-control form-search" type="text" placeholder="Search"
                          aria-label="Search">
                      </form>
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-12 full-width">
                      <form class="form-inline">
                        <input class="form-control form-search" type="text" placeholder="Search"
                          aria-label="Search">
                      </form>
                   </div>
                 </div>
                 <div class="row">
                    <div class="col-md-12 why-sub-row-one">
                          <div class="row">
                              <div class="col-4 col-sm-4">
                                <img src="images/here_5.jpg" height="70" width="70" alt="">
                              </div>
                              <div class="col-4 col-sm-4 text-left">
                                 <p>fsldafklsdf</p>
                                 <p>kdfjslafjdskafl</p>
                              </div>
                              <div class="col-4 col-sm-4 text-right">
                                <p>12<img src="images/ic_like.png" height="20">4<img src="images/ic_dis_like.png"  height="20"><img src="images/ic_delete.png" height="20"></p>
                                <button class="btn-warning btn-sm">Set as accomodation</button>
                              </div>
                          </div>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-md-12 why-sub-row-one">
                          <div class="row">
                              <div class="col-4 col-sm-4">
                                <img src="images/here_5.jpg" height="70" width="70" alt="">
                              </div>
                              <div class="col-4 col-sm-4 text-left">
                                 <p>fsldafklsdf</p>
                                 <p>kdfjslafjdskafl</p>
                              </div>
                              <div class="col-4 col-sm-4 text-right">
                                <p>12<img src="images/ic_like.png" height="20">4<img src="images/ic_dis_like.png"  height="20"><img src="images/ic_delete.png" height="20"></p>
                                <button class="btn-warning btn-sm">Set as accomodation</button>
                              </div>
                          </div>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-md-12 why-sub-row-one">
                          <div class="row">
                              <div class="col-4 col-sm-4">
                                <img src="images/here_5.jpg" height="70" width="70" alt="">
                              </div>
                              <div class="col-4 col-sm-4 text-left">
                                 <p>fsldafklsdf</p>
                                 <p>kdfjslafjdskafl</p>
                              </div>
                              <div class="col-4 col-sm-4 text-right">
                                <p>12<img src="images/ic_like.png" height="20">4<img src="images/ic_dis_like.png"  height="20"><img src="images/ic_delete.png" height="20"></p>
                                <button class="btn-warning btn-sm">Set as accomodation</button>
                              </div>
                          </div>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-md-12 why-sub-row-one">
                          <div class="row">
                              <div class="col-4 col-sm-4">
                                <img src="images/here_5.jpg" height="70" width="70" alt="">
                              </div>
                              <div class="col-4 col-sm-4 text-left">
                                 <p>fsldafklsdf</p>
                                 <p>kdfjslafjdskafl</p>
                              </div>
                              <div class="col-4 col-sm-4 text-right">
                                <p>12<img src="images/ic_like.png" height="20">4<img src="images/ic_dis_like.png"  height="20"><img src="images/ic_delete.png" height="20"></p>
                                <button class="btn-warning btn-sm">Set as accomodation</button>
                              </div>
                          </div>
                    </div>
                 </div>
                  
                 </div>
             </div>
             <div class="col-md-6"></div>
         </div>
     </div>

    
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/jquery-migrate-3.0.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/aos.js"></script>
    <script src="js/bootstrap-datepicker.js"></script> 
    <script src="js/jquery.timepicker.min.js"></script> 
    <script src="js/main.js"></script>
  </body>
</html>